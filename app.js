const tasks = [
  {
    _id: '5d2ca9e2e03d40b326596aa7',
    completed: true,
    body:
      'Occaecat non ea quis occaecat ad culpa amet deserunt incididunt elit fugiat pariatur. Exercitation commodo culpa in veniam proident laboris in. Excepteur cupidatat eiusmod dolor consectetur exercitation nulla aliqua veniam fugiat irure mollit. Eu dolor dolor excepteur pariatur aute do do ut pariatur consequat reprehenderit deserunt.\r\n',
    title: 'Eu ea incididunt sunt consectetur fugiat non.',
  },
  {
    _id: '5d2ca9e29c8a94095c1288e0',
    completed: false,
    body:
      'Aliquip cupidatat ex adipisicing veniam do tempor. Lorem nulla adipisicing et esse cupidatat qui deserunt in fugiat duis est qui. Est adipisicing ipsum qui cupidatat exercitation. Cupidatat aliqua deserunt id deserunt excepteur nostrud culpa eu voluptate excepteur. Cillum officia proident anim aliquip. Dolore veniam qui reprehenderit voluptate non id anim.\r\n',
    title:
      'Deserunt laborum id consectetur pariatur veniam occaecat occaecat tempor voluptate pariatur nulla reprehenderit ipsum.',
  },
  {
    _id: '5d2ca9e2e03d40b3232496aa7',
    completed: true,
    body:
      'Occaecat non ea quis occaecat ad culpa amet deserunt incididunt elit fugiat pariatur. Exercitation commodo culpa in veniam proident laboris in. Excepteur cupidatat eiusmod dolor consectetur exercitation nulla aliqua veniam fugiat irure mollit. Eu dolor dolor excepteur pariatur aute do do ut pariatur consequat reprehenderit deserunt.\r\n',
    title: 'Eu ea incididunt sunt consectetur fugiat non.',
  },
  {
    _id: '5d2ca9e29c8a94095564788e0',
    completed: false,
    body:
      'Aliquip cupidatat ex adipisicing veniam do tempor. Lorem nulla adipisicing et esse cupidatat qui deserunt in fugiat duis est qui. Est adipisicing ipsum qui cupidatat exercitation. Cupidatat aliqua deserunt id deserunt excepteur nostrud culpa eu voluptate excepteur. Cillum officia proident anim aliquip. Dolore veniam qui reprehenderit voluptate non id anim.\r\n',
    title:
      'Deserunt laborum id consectetur pariatur veniam occaecat occaecat tempor voluptate pariatur nulla reprehenderit ipsum.',
  },
];

(function(arrOfTasks) {

const objOfTasks = arrOfTasks.reduce((acc,task)=>{
acc[task._id] = task;
return acc;
}, {})

// UI
const container = document.querySelector('.tasks-list-section .list-group')


// Events
container.addEventListener('click',function(e){
  const el = e.target;
if(el.classList.contains('delete-btn')){

  const id = el.parentElement.dataset.taskid;
   el.parentElement.remove();
   delete objOfTasks[id]
   console.log(id);
   console.log(objOfTasks);
}
})

renderTask()

function renderTask(){
  let fragment = ''
  Object.values(objOfTasks).forEach(task =>{
    // console.log(task.title);
    const template = taskTemplate(task);
    fragment += template;
  })
  container.insertAdjacentHTML('afterbegin',fragment)


}
function taskTemplate({_id,title,body}) {
  return `
  <li class="list-group-item d-flex align-items-center flex-wrap mt-2" data-taskId="${_id}">
          <span> ${title}</span>
          <button class="btn btn-danger ml-auto delete-btn">Delete</button>
          <p class="mt-2 w-100">
            ${body}
          </p>
        </li>
        ` 
}

})(tasks);
